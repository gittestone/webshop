package com.yuan.manage.controller;

import org.springframework.stereotype.Controller;

@Controller
public class TestController {

    public String query(){
        // 另一用户 同时也修改了这一行代码
        System.out.println("这是一个测试方法, 我是一个用户, 另一个用户也在修改这行代码 这样会产生代码冲突");
        // 一个用户 修改了这一行
//        System.out.println("这是一个测试方法, 代码合到一行了");

        System.out.println("回退后再修改并提交"); // 这句是在 回退后 编写的

        System.out.println("这是一个新的内容");

        System.out.println("又一次回退后的新内容");

        System.out.println("这是另一个成员的新内容"); // 另一个人提交的新内容

        // 与另一个用户同时修改同一个方法中的代码  这样就会产生冲突
        System.out.println("同时修改同一个文件");

        System.out.println("我是第三个参与者, 编写的代码");

        System.out.println("测试 git 命令");

       return "";
    }
}
